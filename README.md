## Installation

- Run `composer install` and `npm install` or `yarn`
- Run `yarn and `yarn run dev` to generate assets
- Copy `.env.example` to `.env` and fill your values (`php artisan key:generate`, Twitter keys, database, etc)
- Run `php artisan migrate`, this will migrate one table

```
TWITTER_CONSUMER_KEY=
TWITTER_CONSUMER_SECRET=
TWITTER_ACCESS_TOKEN=
TWITTER_ACCESS_TOKEN_SECRET=
```

## PHP Packages Used
- `abraham/twitteroauth` to interface with the Twitter API
- `beyondcode/laravel-dump-server` to dump values of variables when consuming an API endpoint
- `adamwathan/laravel-preset` because TailwindCSS is amazing

## NPM Packages Used
- `tailwindcss` a utility based CSS framework. 
- `momentjs` because formatting time in JavaScript is awful