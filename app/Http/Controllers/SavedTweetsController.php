<?php

namespace App\Http\Controllers;

use App\Tweet;
use Illuminate\Http\Request;

class SavedTweetsController extends Controller
{

    public function index()
    {
        $tweets = Tweet::all(); //In real life we'd paginate
        return response()->json($tweets, 201);
    }

    public function store()
    {   
        $tweet = request('tweet');

        $savedTweet = Tweet::create([
            'tweet_id' => $tweet['id'],
            'username' => $tweet['user']['screen_name'],
            'text' => $tweet['text'],
            'name' => $tweet['user']['name'],
            'created_at' => $tweet['created_at'],
            'payload' => collect($tweet), //collected so we can access as an array
        ]);

        return response()->json($savedTweet, 201);
        
    }

    public function delete($tweet_id)
    {
        Tweet::where('tweet_id', $tweet_id)->delete();

        return response(204);
    }

    public function show($tweet_id)
    {
        return response()->json(Tweet::where('tweet_id', $tweet_id)->exists(), 200);
    }
}
