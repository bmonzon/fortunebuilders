<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\TweetResource;
use Abraham\TwitterOAuth\TwitterOAuth;

class SearchTweetsController extends Controller
{
    public function search(TwitterOAuth $twitter)
    {
        if(empty(request('q'))) {
            return response()->json([
                ['message' => 'You must include a search query']
            ], 403);
        }

        $statuses = $twitter->get("search/tweets", ["q" => request('q')]);
        
        return $statuses->statuses;
    }
}
