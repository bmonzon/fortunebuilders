/**
 * Format the given date.
 */
Vue.filter('date', value => {
    return moment(value, 'ddd MMM DD HH:mm:ss ZZ YYYY').local().format('MMM Do')
});


/**
 * Format the given date as a timestamp.
 */
Vue.filter('datetime', value => {
    return moment(value, 'ddd MMM DD HH:mm:ss ZZ YYYY').local().format('MMMM Do, YYYY h:mm A');
});


/**
 * Format the given date into a relative time.
 */
Vue.filter('relative', value => {
    return moment(value, 'ddd MMM DD HH:mm:ss ZZ YYYY').local().locale('en-short').fromNow();
});