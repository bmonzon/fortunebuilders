<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('search', 'SearchTweetsController@search');

Route::get('tweets', 'SavedTweetsController@index');
Route::post('tweets', 'SavedTweetsController@store');
Route::delete('tweets/{tweet_id}', 'SavedTweetsController@delete');
Route::get('tweets/{tweet_id}', 'SavedTweetsController@show');